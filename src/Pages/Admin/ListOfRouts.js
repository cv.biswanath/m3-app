import * as React from "react";
import { get, del, put } from "../../helper/apiHelper";
import ListOfRoutsView from "../../View/Admin/ListOfRouts";
import { validateResponseAdmin } from "../../helper/validateResponse";
import swal from "sweetalert";

export default function ListOfRouts() {
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [selectedRouteForAssign, setSelectedRouteForAssign] =
    React.useState(null);
  const [openUserModal, setOpenUserModal] = React.useState(false);
  const [routsData, setRoutsData] = React.useState(null);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const fetchAllRouts = async () => {
    setLoading(true);
    const response = await get("/admin/route?search=&page&limit");
    if (validateResponseAdmin(response)) {
      setRoutsData(response?.data?.rows);
    }
    setLoading(false);
  };

  const deleteRoute = async (id) => {
    swal({
      title: "Are you sure?",
      text: "You want to delete the route",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then(async (willDelete) => {
      if (willDelete) {
        setLoading(true);
        const response = await del(`/admin/route/${id}`);
        if (validateResponseAdmin(response)) {
          swal("Success!", "Route deleted successfully!", "success").then(
            (value) => {
              fetchAllRouts();
            }
          );
        }
        setLoading(false);
      }
    });
  };

  const updateRouteStatus = async (routeId) => {
    setLoading(true);
    const response = await put("/admin/route/toggleRouteStatus", { routeId });
    if (validateResponseAdmin(response)) {
      console.log(response);
      swal("Success!", "Route status successfully changed!", "success").then(
        (value) => {
          fetchAllRouts();
        }
      );
    }
  };

  React.useEffect(() => {
    fetchAllRouts();
  }, []);

  const assignUser = (id) => {
    setOpenUserModal(true);
    setSelectedRouteForAssign(id);
  };
  return (
    <>
      {!loading ? (
        <ListOfRoutsView
          open={open}
          handleClose={handleClose}
          openUserModal={openUserModal}
          setOpenUserModal={setOpenUserModal}
          selectedRouteForAssign={selectedRouteForAssign}
          handleOpen={handleOpen}
          routsData={routsData}
          assignUser={assignUser}
          deleteRoute={deleteRoute}
          updateRouteStatus={updateRouteStatus}
        />
      ) : (
        <center>
          <div className="loader" style={{ margin: "5rem" }}></div>
        </center>
      )}
    </>
  );
}
