import React from "react";
import Map from "../components/Map";
import CircleViewDetailsModal from "../components/CircleViewDetailsModal";
import CaptureData from "../components/CaptureData";
import { validateResponseUser } from "../helper/validateResponse";
import "../css/dutylist.css";
import VisitTable from "../components/VisitTable";
import HorizontalPlaceScroll from "../components/HorizontalPlaceScroll";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import FlipCameraAndroidIcon from "@mui/icons-material/FlipCameraAndroid";
import Button from "@mui/material/Button";
import { get } from "../helper/apiHelper";
import SearchPlaces from "../components/SearchPlaces";
import Compass from "../components/Compass";
import Fab from "@mui/material/Fab";

import { useDispatch, useSelector } from "react-redux";
import {
  setLocationsData,
  setLocationsTypeData,
} from "../redux/slices/site/siteSlice";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

// const MapWrapped = withScriptjs(withGoogleMap(Map));
let watchId;

function Home() {
  const locationData = useSelector((state) => state.site.locations);
  const locationType = useSelector((state) => state.site.locationType);
  const dispatch = useDispatch();

  const [currentLocation, setCurrentLocation] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [clickedPlace, setClickedPlace] = React.useState(false);
  const [locationsFiltered, setLocationsFiltered] = React.useState(null);
  const [user, setUser] = React.useState(null);
  const [value, setValue] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const [reCenterLoocation, setReCenterLoocation] = React.useState(null);
  const [routeData, setRouteData] = React.useState(null);
  const [clickedLocationType, setClickedLocationType] = React.useState(null);

  const [locations, setLocations] = React.useState(locationData);
  const [locationTypes, setLocationTypes] = React.useState(locationType);

  const locationTypeClickHandler = (id) => {
    setLoading(true);
    const getAllLocation = locations && [...locations];
    if (clickedLocationType !== id) {
      setClickedLocationType(id);
      const filterData = getAllLocation?.filter(
        (data) => data?.locationType?.id === Number(id)
      );
      setLocationsFiltered(filterData);
      console.log("---filterData->", filterData);
    } else {
      setClickedLocationType(null);
      setLocationsFiltered(getAllLocation);
    }
    setTimeout(() => {
      setLoading(false);
    }, 100);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const fetchData = async () => {
    setLoading(true);
    const user = localStorage.getItem("x-user-data");
    if (user) {
      setUser(JSON.parse(user));
    }
    const responseLocationType = await get(`/getLocationTypes`);
    if (validateResponseUser(responseLocationType)) {
      setLocationTypes(responseLocationType?.data);
      dispatch(setLocationsTypeData(responseLocationType?.data));
    }

    const responseRoutes = await get(`/user/getRoutes`);
    if (validateResponseUser(responseRoutes)) {
      setRouteData(responseRoutes?.data);
      const allLocations = responseRoutes?.data?.flatMap(
        (route) => route.locations
      );
      setLocations(allLocations);
      dispatch(setLocationsData(allLocations));
    }
    setLoading(false);
  };

  React.useEffect(() => {
    if (!locationData) {
      fetchData();
    }
  }, []);

  const handleOpen = (text) => {
    setOpen(true);
    setClickedPlace(text);
  };

  const handleClose = () => setOpen(false);

  // --------------------------------------------------Live tracking ------------

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const trackFetchLocation = () => {
    watchId = navigator.geolocation.watchPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        const positionData = { lat: latitude, lng: longitude };
        setReCenterLoocation(positionData);
        setCurrentLocation(positionData);
      },
      (error) => console.log(error),
      { enableHighAccuracy: true, maximumAge: 20000, timeout: 10000 }
    );
  };

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      trackFetchLocation();
    }, 3000);
    return () => {
      navigator.geolocation.clearWatch(watchId);
      clearInterval(intervalId);
    };
  }, [trackFetchLocation]);

  const getCurrentLocation = () => {
    setLoading(true);
    navigator.geolocation.getCurrentPosition((position) => {
      const { latitude, longitude } = position.coords;
      setCurrentLocation({
        lat: latitude,
        lng: longitude,
      });
      setReCenterLoocation({
        lat: latitude,
        lng: longitude,
      });
      setLoading(false);
    });
  };

  React.useEffect(() => {
    let animationId;
    function keepScreenOn() {
      animationId = window.requestAnimationFrame(keepScreenOn);
    }
    keepScreenOn();
    const handleScreenChange = () => {
      if (window.screen && window.screen.keepAwake) {
        window.screen.keepAwake = true;
      }
    };
    handleScreenChange();
    document.addEventListener("visibilitychange", handleScreenChange);
    return () => {
      window.cancelAnimationFrame(animationId);
      document.removeEventListener("visibilitychange", handleScreenChange);
    };
  }, []);

  const findPlace = async (destination, destinationId = null) => {
    const source = `${reCenterLoocation?.lat},${reCenterLoocation?.lng}`;
    window.location.href = `#/navigation/${source}/${destination}?sourcePlaceID=&destinationPlaceId=${destinationId}`;
  };

  console.log("---locationDataRedux->", locationData);
  return (
    <>
      <CircleViewDetailsModal
        open={open}
        onClose={handleClose}
        clickedPlace={clickedPlace}
        currentLocation={reCenterLoocation}
      />

      {!loading && locations && reCenterLoocation ? (
        <>
          <Box sx={{ width: "100%" }} style={{ marginBottom: "100px" }}>
            <Box>
              <div className="top-hscroll">
                {reCenterLoocation && (
                  <HorizontalPlaceScroll
                    locations={locations}
                    findPlace={findPlace}
                  />
                )}
              </div>
              {locations && (
                <SearchPlaces
                  locations={locations}
                  destinationPlaceId={null}
                  sourcePlaceID={null}
                />
              )}
            </Box>
            <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <Tabs
                value={value}
                onChange={handleChange}
                aria-label="basic tabs example"
              >
                <Tab
                  label={window.site_text(`pages.map.map_view`)}
                  {...a11yProps(0)}
                />
                <Tab
                  label={`${window.site_text(`pages.map.list_view`)} (${
                    clickedLocationType
                      ? locationsFiltered?.length
                      : locations?.length
                  })`}
                  {...a11yProps(1)}
                />
              </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
              <div style={{ width: "auto", height: "60vh" }}>
                {!loading && locations && reCenterLoocation ? (
                  <Map
                    locations={
                      clickedLocationType ? locationsFiltered : locations
                    }
                    defaultCenter={reCenterLoocation}
                    handleOpen={handleOpen}
                    reCenterLoocation={reCenterLoocation}
                  />
                ) : (
                  <center
                    style={{
                      width: "auto",
                      height: "60vh",
                      paddingTop: "10rem",
                    }}
                  >
                    <div className="loader"></div>
                  </center>
                )}
                <div className="container grey-location">
                  <div className="row routecard">
                    <div className="col-4">
                      <p className="text-black">
                        {" "}
                        {window.site_text(`pages.map.re_center`)}
                      </p>
                    </div>
                    <div className="col-5">
                      {/* <Fab
                        color="secondary"
                        aria-label="add"
                        style={{
                          background: "#ad0004",
                        }}
                      >
                        <Compass />
                      </Fab> */}
                    </div>

                    <div
                      className="col-3"
                      style={{ marginTop: "-4rem" }}
                      onClick={() => getCurrentLocation()}
                    >
                      <span
                        className="red-circle"
                        style={{ color: "white", fontWeight: "bold" }}
                      >
                        <FlipCameraAndroidIcon />
                        {/* <img src="../images/Vector.png" alt="" /> */}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </TabPanel>
            <TabPanel value={value} index={1} style={{ padding: 10 }}>
              <VisitTable
                locations={clickedLocationType ? locationsFiltered : locations}
                handleOpen={handleOpen}
              />
            </TabPanel>
          </Box>
          <br />
          <br />
          <div className="bottom-hscroll">
            <div className="hscroll-box">
              {locationTypes?.map((data, index) => {
                return (
                  <Button
                    className={
                      clickedLocationType === data?.id
                        ? "hscroll-button active"
                        : "hscroll-button"
                    }
                    variant="outlined"
                    size="small"
                    onClick={() => locationTypeClickHandler(data?.id)}
                  >
                    {data?.name}
                    {/* <div className="time-icon">
                        <img
                          src="../images/icon-time.png"
                          alt=""
                          style={{ marginLeft: 5 }}
                        />
                      </div> */}
                  </Button>
                );
              })}
            </div>
          </div>
          <br />
        </>
      ) : (
        <center style={{ width: "auto", height: "85vh", paddingTop: "10rem" }}>
          <div className="loader"></div>
        </center>
      )}
    </>
  );
}

export default Home;
