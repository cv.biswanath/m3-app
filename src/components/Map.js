/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { MAP_KEY } from "../config";

export default function Map({ locations, handleOpen, reCenterLoocation }) {
  const [map, setMap] = React.useState(null);
  const [marker, setMarker] = React.useState(null);

  useEffect(() => {
    const script = document.createElement("script");
    script.src = `https://maps.googleapis.com/maps/api/js?key=${MAP_KEY}`;
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);

    script.addEventListener("load", () => {
      initMap();
    });

    return () => {
      document.body.removeChild(script);
    };
  }, [locations]);

  useEffect(() => {
    if (map && marker) {
      reCenterLoocation && marker && marker.setPosition(reCenterLoocation);
    }
  }, [reCenterLoocation]);

  const initMap = () => {
    const map = new window.google.maps.Map(document.getElementById("map"), {
      // center: reCenterLoocation, // Initial position
      center: { lat: locations[0]?.lat, lng: locations[0]?.long }, // Initial position
      zoom: 17,
    });

    const marker =
      reCenterLoocation &&
      new window.google.maps.Marker({
        position: reCenterLoocation, // Marker position
        icon: {
          url: `https://maps.google.com/mapfiles/kml/paddle/purple-stars.png`,
        },
        map, // Map instance
        label: {
          text: "You are here",
          color: "black",
          fontSize: "12px",
          fontWeight: "bold",
        },
      });

    locations &&
      locations.forEach((val) => {
        let color;

        if (val?.isVisited) {
          color = "green";
        } else {
          color = "#FF0000";
        }

        const circle = new window.google.maps.Marker({
          position: { lat: val?.lat, lng: val?.long },
          map,
          label: {
            text: val?.name,
            color: "black",
            fontSize: "12px",
            fontWeight: "bold",
          },
        });

        new window.google.maps.Circle({
          strokeColor: "#000000",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: color,
          fillOpacity: 0.35,
          map,
          center: { lat: val?.lat, lng: val?.long },
          radius: val?.radius,
        });

        circle.addListener("click", () => {
          handleOpen(val);
        });
      });

    setMap(map);
    setMarker(marker);
  };

  return <div id="map" style={{ width: "100%", height: "550px" }} />;
}
