import React from "react";

export default function HorizontalPlaceScroll({ locations, findPlace }) {
  return (
    <div className="hscroll-box" style={{ overflowX: "auto" }}>
      {locations?.map((data) => (
        <div
          className="hscroll-hldr"
          onClick={() => findPlace(`${data?.lat},${data?.long}`, data?.id)}
        >
          <div className="hscroll-text">{data?.name}</div>
          {/* <div className="time-icon">
            <img src="../images/icon-time.png" alt="" />
          </div> */}
        </div>
      ))}
    </div>
  );
}
