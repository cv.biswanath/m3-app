import * as React from "react";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import MapForm from "../../components/MapForm";
import TextField from "@mui/material/TextField";
import ListOfRoutes from "../../components/ListOfRoutes";
import AutocompliteInput from "../../components/AutocompliteInput";
import AddCircleMap from "./AddCircleMap";
import { validateResponseUser } from "../../helper/validateResponse";
import { get } from "../../helper/apiHelper";

const AddCircleToRoutsView = ({
  open,
  handleClose,
  selectLocation,
  updatedPointer,
  setName,
  setDescription,
  radius,
  setRadius,
  addPlace,
  routeData,
  handleClick,
  choosedLocation,
  deletLocation,
  handleMapClick,
  selectedPoliceStationCallback,
  loding,
}) => {
  const [markerLocation, setSelectMarkerLocation] = React.useState(null);
  const [locationType, setLocationsType] = React.useState(null);
  const [loading, setLoading] = React.useState(false);

  const getCurrentLocation = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      const { latitude, longitude } = position.coords;
      setSelectMarkerLocation({
        lat: latitude,
        lng: longitude,
      });
    });
  };

  const fetchData = async () => {
    setLoading(true);

    const responseLocationType = await get(`/getLocationTypes`);
    if (validateResponseUser(responseLocationType)) {
      setLoading(false);
      setLocationsType(responseLocationType?.data);
    } else {
      setLoading(false);
    }
  };

  let watchId;

  React.useEffect(() => {
    getCurrentLocation();
    fetchData();
  }, []);

  const trackFetchLocation = () => {
    watchId = navigator.geolocation.watchPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        const positionData = { lat: latitude, lng: longitude };
        setSelectMarkerLocation(positionData);
      },
      (error) => console.log(error),
      { enableHighAccuracy: true, maximumAge: 20000, timeout: 10000 }
    );
  };

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      trackFetchLocation();
    }, 3000);
    return () => {
      navigator.geolocation.clearWatch(watchId);
      clearInterval(intervalId);
    };
  }, [trackFetchLocation]);

  return (
    <>
      <h2 style={{ padding: 12, marginTop: "2rem" }}>
        <b>Add Circles to routes</b>
      </h2>

      {open ? (
        <>
          <Box>
            {selectLocation && (
              <MapForm
                markers={null}
                updatedPointer={updatedPointer}
                choosedLocation={choosedLocation}
              />
            )}
            <br />
            <br />
            {!loading ? (
              <AutocompliteInput
                data={locationType}
                onchangeCallback={selectedPoliceStationCallback}
                label="Select type"
              />
            ) : (
              <b>Please wait...</b>
            )}

            <TextField
              id="outlined-basic"
              label="Enter Name"
              variant="outlined"
              style={{ marginTop: 10, width: "100%" }}
              onChange={(e) => setName(e.target.value)}
            />
            <TextField
              id="outlined-basic"
              label="Enter Description"
              variant="outlined"
              style={{ marginTop: 10, width: "100%" }}
              onChange={(e) => setDescription(e.target.value)}
            />
            <TextField
              id="outlined-basic"
              label="Circle center"
              variant="outlined"
              value={JSON.stringify(selectLocation)}
              style={{ marginTop: 10, width: "100%", display: "none" }}
              disabled
            />
            <br />
            <TextField
              id="outlined-basic"
              label="Circle Radius (in meters)"
              variant="outlined"
              style={{ marginTop: 10, width: "100%" }}
              onChange={(e) => setRadius(e.target.value)}
              defaultValue={radius}
            />
            <br />
            <br />
            {!loding ? (
              <>
                <button
                  type="button"
                  onClick={() => addPlace()}
                  className="admin-button"
                >
                  Add Circle
                </button>
                <button
                  type="button"
                  onClick={handleClose}
                  className="admin-close-button"
                >
                  Close
                </button>
                <br />
                <br />
              </>
            ) : (
              <center style={{ width: "auto" }}>
                <div className="loader"></div>
              </center>
            )}
          </Box>
        </>
      ) : (
        <>
          {routeData && (
            <>
              <AddCircleMap
                defaultCenter={{
                  lat: routeData?.centerLat,
                  lng: routeData?.centerLong,
                }}
                handleMapClick={handleMapClick}
                markerLocation={markerLocation}
                handleClick={handleClick}
                choosedLocation={choosedLocation}
              />

              <div style={{ margin: 10 }}>
                <ListOfRoutes
                  routeData={choosedLocation}
                  deletLocation={deletLocation}
                />

                <br />
                <div
                  className="login-button"
                  onClick={() =>
                    window.location.replace("#/admin/list-of_routs")
                  }
                >
                  <button type="button" className="btn">
                    <div className="text">
                      <h6>Done</h6>
                    </div>
                  </button>
                </div>
                <br />
                <br />
                <br />
              </div>
            </>
          )}
        </>
      )}
    </>
  );
};

export default AddCircleToRoutsView;
