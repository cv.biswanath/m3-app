import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isAuth: false,
  isAdmin: false,
  loading: false,
  locationType: null,
  locations: null,
};

export const configSlice = createSlice({
  name: "config",
  initialState,
  reducers: {
    updateAuthState: (state, action) => {
      state.isAuth = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setLocationsTypeData: (state, action) => {
      state.locationType = action.payload;
    },
    setLocationsData: (state, action) => {
      state.locations = action.payload;
    },
  },
});

export const {
  updateAuthState,
  setLoading,
  setLocationsTypeData,
  setLocationsData,
} = configSlice.actions;

export default configSlice.reducer;
